using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireplace : MonoBehaviour
{
    [SerializeField] Light fireLight;

    private int blinkDelay;
    private bool off = false;

    // Update is called once per frame
    void Update()
    {
        if (!off)
        {
            turnOff();
        }
        
    }

    private void turnOff()
    {
        fireLight.intensity = Random.Range(4f, 5f);

        Invoke("turnOn", 5f);
    }

    private void turnOn()
    {
        off = false;
    }
}
